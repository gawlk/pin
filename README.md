![preview](documents/images/preview.png)

# Description

`pin` is a very simple library that makes keeping track of what is going on in your bash scripts much easier.

Bonus: It has colors !

# How to install ? 

## Instructions

Write in your terminal:
```bash
git clone "https://gitlab.com/gawlk/pin.git"
cd pin
bash install.sh
```

# How to use ?

## Add the library

Add the following line at the top of your bash scripts:
```bash
. pin
```

## Manual

Example: `pin.info "Description"` or `pin.info "title" "Description"`

pins:
- `pin.info` : Display an info pin
- `pin.debug`: Display a debug pin (default: hidden)
- `pin.warning`: Display a warning pin
- `pin.error`: Display an error pin and quit
- `pin.quit`: Display an info pin and quit

Setters:
- `pin.loud`: Show info pins
- `pin.quiet`: Hide info pins
- `pin.dev`: Show debug pins
- `pin.casual`: Hide debug pins

Getters:
- `pin.is_loud`: Return `true` if info pins are visible
- `pin.is_quiet`: Return `true` if info pins are hidden
- `pin.is_dev`: Return `true` if debug pins are visible
- `pin.is_casual`: Return `true` if debug pins are hidden
