#!/usr/bin/env bash
#
#    _|x  ||''. '||'  |\  |
#   |X|   ||--'  ||   ||\ |
#  /      ||    .||.  || \|
#
# Description:              A minimal Bash logging library
# Dependencies:             none
# Optionnal dependencies:   none
# Author:                   gawlk
# Contributors:             none

# ---
# TODO
# ---

# ---
# IMPORT
# ---

# ---
# SETUP
# ---

# ---
# VARIABLES
# ---

# Background
bblk="\\e[40m"
bred="\\e[41m"

# Foreground
fblk="\\e[30m"
fgry="\\e[90m"
fred="\\e[31m"
fgrn="\\e[32m"
fblu="\\e[34m"
frst="\\e[0m"

# ---
# LOCAL
# ---

pin.print() {
    local kind="$1"
    local fgrd="$2"
    local bgrd="$3"
    local title="$4"
    local description="${@:5}"
    [[ -z "$title" && -z "$description" ]] && title="title" && description="Description"
    
    printf "${fgry}[ ${bgrd}${fgrd}${kind}${bblk} ${fgry}]  "
    [[ -z "$description" ]] && printf "${frst}${title}\n" || printf "${fred}${title}${fgry}: ${frst}${description}\n"
}

# ---
# GLOBAL
# ---

# Getters
pin.is_quiet()  { [[ pin_quiet == true ]]; }
pin.is_loud()   { [[ pin_quiet == false ]]; }
pin.is_dev()    { [[ pin_dev == true ]]; }
pin.is_casual() { [[ pin_dev == false ]]; }

# Logs
pin.info()      { [[ $pin_quiet == false ]] && pin.print "  info " "$fblu" "$bblk" "$1" "${@:2}"; }
pin.debug()     { [[ $pin_dev == true ]] && pin.print " debug " "$fgrn" "$bblk" "$1" "${@:2}"; }
pin.warning()   { pin.print "warning" "$fred" "$bblk" "$1" "${@:2}"; }
pin.error()     { pin.print " error " "$fblk" "$bred" "$1" "${@:2}" && exit 1; }
pin.quit()      { pin.info "$1" "${@:2}" && exit 0; }

# Setters
pin.quiet()     { pin_quiet=true; }
pin.loud()      { pin_quiet=false; }
pin.dev()       { pin_dev=true; }
pin.casual()    { pin_dev=false; }

# ---
# MAIN
# ---

pin.main() {
    # Show info
    pin.loud

    # Hide debug
    pin.casual
}

# ---
# LAUNCH
# ---

pin.main
